package com.geoquiz.view.menu;

import com.geoquiz.view.button.*;
import com.geoquiz.view.utility.Background;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.label.MenuLabel;
import com.geoquiz.view.label.LabelsBuilder;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.Label;

/**
 * The scene where user can choose game modality.
 */
public class ModeScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 350;
    private static final double POS_3_Y = 200;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;
    private static final double LABEL_FONT = 35;
    private static final double OPACITY = 0.5;
    private static final Text BUTTON_PRESSED = new Text();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public ModeScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MenuLabel userLabel = LabelsBuilder.buildUserLabel();

        final Button back = ButtonsFactory.createButton(Buttons.INDIETRO.toString());
        final Button classic = ButtonsFactory.createButton(ButtonsGameModes.CLASSICA.toString());
        final Button challenge = ButtonsFactory.createButton(ButtonsGameModes.SFIDA.toString());
        final Button training = ButtonsFactory.createButton(ButtonsGameModes.ALLENAMENTO.toString());

        Label label = new Label();
        if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.CAPITALI.toString())) {
            label.setText("Sai indicare la capitale di ciascun paese?\nScegli prima la modalità di gioco!");
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.MONUMENTI.toString())) {
            label.setText(
                    "Sai indicacare dove si trovano questi famosi monumenti?\nScegli prima la modalità di gioco!");
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.BANDIERE.toString())) {
            label.setText("Sai indicare i paesi in base alla bandiera nazionale?\nScegli prima la modalità di gioco!");
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.CUCINA.toString())) {
            label.setText("Sai indicare di quali paesi sono tipici questi piatti?\nScegli prima la modalità di gioco!");
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.VALUTE.toString())) {
            label.setText(
                    "Sai indicare qual e' la valuta adottata da ciascun paese?\nScegli prima la modalità di gioco!");
        }

        label.setFont(Font.font("Italic", FontWeight.BOLD, LABEL_FONT));

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);
        vbox.getChildren().addAll((Node) classic, (Node) challenge, (Node) training);
        VBox vbox2 = new VBox();
        vbox2.getChildren().add((Node) back);
        VBox vbox3 = new VBox();
        vbox3.getChildren().add(label);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);
        vbox3.setTranslateX(POS_2_X);
        vbox3.setTranslateY(POS_3_Y);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });

        ((Node) classic).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            ModeScene.BUTTON_PRESSED.setText(ButtonsGameModes.CLASSICA.toString());
            getGameModality();
            if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.CAPITALI.toString())
                    || CategoryScene.getCategoryPressed().equals(ButtonsCategory.MONUMENTI.toString())) {
                mainStage.setScene(new LevelScene(mainStage));
            } else {
                try {
                    mainStage.setScene(new QuizGamePlay(mainStage));
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        });

        ((Node) challenge).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            ModeScene.BUTTON_PRESSED.setText(ButtonsGameModes.SFIDA.toString());
            getGameModality();
            try {
                mainStage.setScene(new QuizGamePlay(mainStage));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        ((Node) training).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            ModeScene.BUTTON_PRESSED.setText(ButtonsGameModes.ALLENAMENTO.toString());
            getGameModality();
            try {
                mainStage.setScene(new QuizGamePlay(mainStage));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.setBackgroundImage(), Background.createBackground(),
                Background.getLogo(), vbox, vbox2, vbox3, (Node) userLabel);

        this.setRoot(panel);

    }

    /**
     * @return game modality.
     */
    static String getGameModality() {
        return BUTTON_PRESSED.getText();
    }

    /**
     * @return background image for own category.
     */
    static ImageView setBackgroundImage() {
        if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.CAPITALI.toString())) {
            final ImageView img;
            img = Background.getCategoryImage("/images/capitali.jpg");
            img.setOpacity(OPACITY);
            return img;
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.MONUMENTI.toString())) {
            final ImageView img;
            img = Background.getCategoryImage("/images/monumenti.jpg");
            img.setOpacity(OPACITY);
            return img;
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.VALUTE.toString())) {
            final ImageView img;
            img = Background.getCategoryImage("/images/valute.jpg");
            img.setOpacity(OPACITY);
            return img;
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.CUCINA.toString())) {
            final ImageView img;
            img = Background.getCategoryImage("/images/cucina.jpg");
            img.setOpacity(OPACITY);
            return img;
        } else if (CategoryScene.getCategoryPressed().equals(ButtonsCategory.BANDIERE.toString())) {
            final ImageView img;
            img = Background.getCategoryImage("/images/bandiere.jpg");
            img.setOpacity(OPACITY);
            return img;
        } else {
            return Background.getImage();
        }
    }

}
