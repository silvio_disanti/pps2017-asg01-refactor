package com.geoquiz.view.menu;

import com.geoquiz.view.button.Button;
import com.geoquiz.view.button.ButtonsFactory;
import com.geoquiz.view.label.LabelsBuilder;
import com.geoquiz.view.label.MenuLabels;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ConfirmBox;
import com.geoquiz.view.utility.ExitProgram;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.label.MenuLabel;

import java.io.IOException;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
public class LoginMenuScene extends Scene {

    private static final double TF_OPACITY = 0.7;
    private static final double TF_FONT = 25;
    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 525;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_3_X = 450;
    private static final double POS_3_Y = 300;
    private static final double FONT = 35;

    private final TextField tfUser = new TextField();
    private final PasswordField tfPass = new PasswordField();
    private boolean loginData;
    private static String username;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws IOException
     *             for exception login.
     */
    public LoginMenuScene(final Stage mainStage) throws IOException {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MenuLabel userLabel;
        final MenuLabel passwordLabel;

        final Button enterButton;
        final Button newPlayerButton;
        final Button exitButton;

        final Account account = new AccountImpl("account.txt");

        LabelsBuilder labelsBuilder = LabelsBuilder.newBuilder()
                .color(Color.WHITE)
                .fontDimension(FONT);

        exitButton = ButtonsFactory.createButton(Buttons.ESCI.toString());
        enterButton = ButtonsFactory.createButton(Buttons.ACCEDI.toString());
        newPlayerButton = ButtonsFactory.createButton(Buttons.REGISTRATI.toString());

        userLabel = labelsBuilder.name(MenuLabels.USERNAME.toString()).build();
        passwordLabel = labelsBuilder.name(MenuLabels.PASSWORD.toString()).build();

        tfUser.getFont();
        tfUser.setFont(Font.font(TF_FONT));
        tfUser.setOpacity(TF_OPACITY);
        tfUser.setPromptText("Username");

        tfPass.getFont();
        tfPass.setFont(Font.font(TF_FONT));
        tfPass.setOpacity(TF_OPACITY);
        tfPass.setPromptText("Password");

        VBox vbox1 = new VBox();
        vbox1.setTranslateX(POS_1_X);
        vbox1.setTranslateY(POS_1_Y);
        VBox vbox2 = new VBox(20);
        vbox2.setTranslateX(POS_2_X);
        vbox2.setTranslateY(POS_2_Y);
        VBox vbox3 = new VBox(10);
        vbox3.setTranslateX(POS_3_X);
        vbox3.setTranslateY(POS_3_Y);

        vbox1.getChildren().addAll((Node) enterButton, (Node) newPlayerButton, (Node) exitButton);
        vbox2.getChildren().addAll((Node) userLabel, (Node) passwordLabel);
        vbox3.getChildren().addAll(tfUser, tfPass);

        ((Node) enterButton).setOnMouseClicked(event -> {

            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            try {
                loginData = true;
                account.checkLogin(getUser(), getPass());
            } catch (IllegalArgumentException e) {
                loginData = false;
                ConfirmBox.getBox();
                final Alert alert = ConfirmBox.getAlert("Errore! Username o password errati!", Color.BLACK);
                alert.show();
                e.printStackTrace();
            }
            try {
                if (loginData) {
                    username = tfUser.getText();
                    mainStage.setScene(new MainMenuScene(mainStage));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) exitButton).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            ExitProgram.exitProgram(mainStage);
        });

        ((Node) newPlayerButton).setOnMouseClicked(event -> {

            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            try {
                mainStage.setScene(new AccountRegisterScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox1, vbox2, vbox3,
                Background.getLogo());

        this.setRoot(panel);

    }

    /**
     * @return username authentication.
     */
    public String getUser() {
        return tfUser.getText();
    }

    /**
     * @return password authentication.
     */
    private String getPass() {
        return tfPass.getText();
    }

    /**
     * @return username.
     */
    public static String getUsername() {
        return username;
    }

}
