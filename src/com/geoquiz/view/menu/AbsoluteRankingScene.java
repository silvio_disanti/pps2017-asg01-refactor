package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.geoquiz.controller.ranking.Ranking;
import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.*;
import com.geoquiz.view.label.LabelsBuilder;
import com.geoquiz.view.label.MenuLabel;
import com.geoquiz.view.utility.Background;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The ranking scene where user can see other user's records.
 */
public class AbsoluteRankingScene extends Scene {

    private static final double POS_1_X = 20;
    private static final double POS_Y_BACK = 650;
    private static final double CATEGORY_FONT = 35;
    private static final double RANK_FONT = 25;
    private static final double POS_X_CATEGORY_BOX = 50;
    private static final double POS_Y_CATEGORY_BOX = 200;
    private static final double POS_X_CATEGORY_BOX_2 = 650;
    private static final double POS_Y_CATEGORY_BOX_2 = 75;
    private static final double POS_X_CAPITALS_BOX = 300;
    private static final double POS_Y_CAPITALS_BOX = 205;
    private static final double POS_X_MONUMENTS_BOX = 300;
    private static final double POS_Y_MONUMENTS_BOX = 450;
    private static final double POS_X_FLAGS_BOX = 850;
    private static final double POS_Y_FLAGS_BOX = 80;
    private static final double POS_X_CURRENCIES_BOX = 850;
    private static final double POS_Y_CURRENCIES_BOX = 325;
    private static final double POS_X_DISHES_BOX = 850;
    private static final double POS_Y_DISHES_BOX = 570;
    private static final double TITLE_FONT = 95;
    private static final String ARROW = " -> ";

    private final MenuLabel title;

    private final MenuLabel capitalsEasy;
    private final MenuLabel capitalsMedium;
    private final MenuLabel capitalsHard;
    private final MenuLabel capitalsChallenge;
    private final MenuLabel monumentsEasy;
    private final MenuLabel monumentsMedium;
    private final MenuLabel monumentsHard;
    private final MenuLabel monumentsChallenge;
    private final MenuLabel flagsClassic;
    private final MenuLabel flagsChallenge;
    private final MenuLabel currenciesClassic;
    private final MenuLabel currenciesChallenge;
    private final MenuLabel dishesClassic;
    private final MenuLabel dishesChallenge;

    private final Ranking ranking = Ranking.getInstance();

    private Map<Pair<String, String>, Pair<String, Integer>> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public AbsoluteRankingScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        LabelsBuilder labelsBuilder = LabelsBuilder.newBuilder();

        final MenuLabel capitals;
        final MenuLabel monuments;
        final MenuLabel flags;
        final MenuLabel currencies;
        final MenuLabel dishes;

        title = labelsBuilder
                .name("Classifica")
                .color(Color.BLACK)
                .fontDimension(TITLE_FONT)
                .build();
        try {
            map = this.ranking.getGlobalRanking();
        } catch (ClassNotFoundException | IOException e1) {
            e1.printStackTrace();
        }

        final Button back = ButtonsFactory.createButton(Buttons.INDIETRO.toString());

        LabelsBuilder category = labelsBuilder
                .color(Color.RED)
                .fontDimension(CATEGORY_FONT);

        capitals = category.name(ButtonsCategory.CAPITALI.toString()).build();
        monuments = category.name(ButtonsCategory.MONUMENTI.toString()).build();
        flags = category.name(ButtonsCategory.BANDIERE.toString()).build();
        currencies = category.name(ButtonsCategory.VALUTE.toString()).build();
        dishes = category.name(ButtonsCategory.CUCINA.toString()).build();

        LabelsBuilder level = labelsBuilder
                .color(Color.BLACK)
                .fontDimension(RANK_FONT);

        capitalsEasy = level.name(ButtonsLevels.FACILE.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsLevels.FACILE.toString()))
                .build();

        capitalsMedium = level.name(ButtonsLevels.MEDIO.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsLevels.MEDIO.toString()))
                .build();

        capitalsHard = level.name(ButtonsLevels.DIFFICILE.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsLevels.DIFFICILE.toString()))
                .build();

        capitalsChallenge = level.name(ButtonsGameModes.SFIDA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsGameModes.SFIDA.toString()))
                .build();

        monumentsEasy = level.name(ButtonsLevels.FACILE.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsLevels.FACILE.toString()))
                .build();

        monumentsMedium = level.name(ButtonsLevels.MEDIO.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsLevels.MEDIO.toString()))
                .build();

        monumentsHard = level.name(ButtonsLevels.DIFFICILE.toString() + ARROW + this.getRecordByCategory(
                ButtonsCategory.MONUMENTI.toString(), ButtonsLevels.DIFFICILE.toString()))
                .build();

        monumentsChallenge = level.name(ButtonsGameModes.SFIDA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsGameModes.SFIDA.toString()))
                .build();

        flagsClassic = level.name(ButtonsGameModes.CLASSICA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.BANDIERE.toString(), ButtonsGameModes.CLASSICA.toString()))
                .build();

        flagsChallenge = level.name(ButtonsGameModes.SFIDA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.BANDIERE.toString(), ButtonsGameModes.SFIDA.toString()))
                .build();

        currenciesClassic = level.name(ButtonsGameModes.CLASSICA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.VALUTE.toString(), ButtonsGameModes.CLASSICA.toString()))
                .build();

        currenciesChallenge = level.name(ButtonsGameModes.SFIDA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.VALUTE.toString(), ButtonsGameModes.SFIDA.toString()))
                .build();

        dishesClassic = level.name(ButtonsGameModes.CLASSICA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.CUCINA.toString(), ButtonsGameModes.CLASSICA.toString()))
                .build();

        dishesChallenge = level.name(ButtonsGameModes.SFIDA.toString() + ARROW
                + this.getRecordByCategory(ButtonsCategory.CUCINA.toString(), ButtonsGameModes.SFIDA.toString()))
                .build();

        VBox titleBox = new VBox();
        titleBox.getChildren().add((Node) title);

        VBox capitalsBox = new VBox();
        capitalsBox.getChildren().addAll((Node) capitalsEasy, (Node) capitalsMedium, (Node) capitalsHard,
                (Node) capitalsChallenge);
        VBox monumentsBox = new VBox();
        monumentsBox.getChildren().addAll((Node) monumentsEasy, (Node) monumentsMedium, (Node) monumentsHard,
                (Node) monumentsChallenge);
        VBox currenciesBox = new VBox();
        currenciesBox.getChildren().addAll((Node) currenciesClassic, (Node) currenciesChallenge);
        VBox flagsBox = new VBox();
        flagsBox.getChildren().addAll((Node) flagsClassic, (Node) flagsChallenge);
        VBox dishesBox = new VBox();
        dishesBox.getChildren().addAll((Node) dishesClassic, (Node) dishesChallenge);

        VBox categoryBox = new VBox(200);
        categoryBox.getChildren().addAll((Node) capitals, (Node) monuments);
        VBox categoryBox2 = new VBox(200);
        categoryBox2.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);

        categoryBox.setTranslateX(POS_X_CATEGORY_BOX);
        categoryBox.setTranslateY(POS_Y_CATEGORY_BOX);
        categoryBox2.setTranslateX(POS_X_CATEGORY_BOX_2);
        categoryBox2.setTranslateY(POS_Y_CATEGORY_BOX_2);
        capitalsBox.setTranslateX(POS_X_CAPITALS_BOX);
        capitalsBox.setTranslateY(POS_Y_CAPITALS_BOX);
        monumentsBox.setTranslateX(POS_X_MONUMENTS_BOX);
        monumentsBox.setTranslateY(POS_Y_MONUMENTS_BOX);
        dishesBox.setTranslateX(POS_X_DISHES_BOX);
        dishesBox.setTranslateY(POS_Y_DISHES_BOX);
        flagsBox.setTranslateX(POS_X_FLAGS_BOX);
        flagsBox.setTranslateY(POS_Y_FLAGS_BOX);
        currenciesBox.setTranslateX(POS_X_CURRENCIES_BOX);
        currenciesBox.setTranslateY(POS_Y_CURRENCIES_BOX);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_Y_BACK);
        vbox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, categoryBox,
                categoryBox2, capitalsBox, monumentsBox, flagsBox, currenciesBox, dishesBox, titleBox);

        this.setRoot(panel);
    }

    private String getRecordByCategory(final String category, final String difficulty) {
        final Pair<String, Integer> record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.getX() + " " + "(" + record.getY() + ")";
    }

    /**
     * clear records labels.
     */
    private void clearLabel(MenuLabel menuLabel) {
        menuLabel.setText(menuLabel.getText().substring(0,
                menuLabel.getText().indexOf(" ", menuLabel.getText().indexOf(" ") +1) + 1));
    }

    void clearAllLabels() {

        List<MenuLabel> labels = Arrays.asList(capitalsEasy, capitalsMedium, capitalsHard, capitalsChallenge,
                monumentsEasy, monumentsMedium, monumentsHard, monumentsChallenge, flagsClassic, flagsChallenge,
                currenciesClassic, currenciesChallenge, dishesClassic, dishesChallenge);

        labels.forEach(this::clearLabel);
    }

    /**
     * gets the value of title label.
     * 
     * @return title label.
     */
    MenuLabel getTitle() {
        return title;
    }

    /**
     * gets the value of facilecap label.
     * 
     * @return facilecap label.
     */
    MenuLabel getCapitalsEasy() {
        return capitalsEasy;
    }

    /**
     * gets the value of mediocap label.
     * 
     * @return mediocap label.
     */
    MenuLabel getCapitalsMedium() {
        return capitalsMedium;
    }

    /**
     * gets the value of difficilecap label.
     * 
     * @return difficilecap label.
     */
    MenuLabel getCapitalsHard() {
        return capitalsHard;
    }

    /**
     * gets the value of sfidacap label.
     * 
     * @return sfidacap label.
     */
    MenuLabel getCapitalsChallenge() {
        return capitalsChallenge;
    }

    /**
     * gets the value of facilemon label.
     * 
     * @return facilemon label.
     */
    MenuLabel getMonumentsEasy() {
        return monumentsEasy;
    }

    /**
     * gets the value of mediomon label.
     * 
     * @return mediomon label.
     */
    MenuLabel getMonumentsMedium() {
        return monumentsMedium;
    }

    /**
     * gets the value of difficilemon label.
     * 
     * @return difficilemon label.
     */
    MenuLabel getMonumentsHard() {
        return monumentsHard;
    }

    /**
     * gets the value of sfidamon label.
     * 
     * @return sfidamon label.
     */
    MenuLabel getMonumentsChallenge() {
        return monumentsChallenge;
    }

    /**
     * gets the value of classicaban label.
     * 
     * @return classicaban label.
     */
    MenuLabel getFlagsClassic() {
        return flagsClassic;
    }

    /**
     * gets the value of sfidaban label.
     * 
     * @return sfidaban label.
     */
    MenuLabel getFlagsChallenge() {
        return flagsChallenge;
    }

    /**
     * gets the value of classicaval label.
     * 
     * @return classicaval.
     */
    MenuLabel getCurrenciesClassic() {
        return currenciesClassic;
    }

    /**
     * gets the value of sfidaval label.
     * 
     * @return sfidaval label.
     */
    MenuLabel getCurrenciesChallenge() {
        return currenciesChallenge;
    }

    /**
     * gets the value of classicacuc label.
     * 
     * @return classicacuc label.
     */
    MenuLabel getDishesClassic() {
        return dishesClassic;
    }

    /**
     * gets the value of sfidacuc label.
     * 
     * @return sfidacuc label.
     */
    MenuLabel getDishesChallenge() {
        return dishesChallenge;
    }

    /**
     * gets the controller.
     * 
     * @return controller.
     */
    Ranking getRanking() {
        return this.ranking;
    }
}
