package com.geoquiz.view.menu;

import java.io.IOException;

import com.geoquiz.controller.account.Account;
import com.geoquiz.controller.account.AccountImpl;
import com.geoquiz.view.button.Button;
import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsFactory;
import com.geoquiz.view.label.MenuLabel;
import com.geoquiz.view.label.LabelsBuilder;
import com.geoquiz.view.utility.Background;
import com.geoquiz.view.utility.ExitProgram;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * The scene where user can choose how to do.
 */
public class MainMenuScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_X_INSTRUCTIONS = 900;
    private static final double POS_Y_INSTRUCTIONS = 638;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     * @throws IOException
     *             for exception.
     */
    public MainMenuScene(final Stage mainStage) throws IOException {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final Button play;
        final Button ranking;
        final Button options;
        final Button exit;
        final Button stats;
        final Button instructions;
        final Account account = new AccountImpl("account.txt");

        final MenuLabel userLabel = LabelsBuilder.buildUserLabel();

        play = ButtonsFactory.createButton(Buttons.GIOCA.toString());
        ranking = ButtonsFactory.createButton(Buttons.CLASSIFICA.toString());
        options = ButtonsFactory.createButton(Buttons.OPZIONI.toString());
        exit = ButtonsFactory.createButton(Buttons.ESCI.toString());
        stats = ButtonsFactory.createButton(Buttons.STATISTICHE.toString());
        instructions = ButtonsFactory.createButton(Buttons.ISTRUZIONI.toString());

        VBox instructionsButtonBox = new VBox();
        instructionsButtonBox.setTranslateX(POS_X_INSTRUCTIONS);
        instructionsButtonBox.setTranslateY(POS_Y_INSTRUCTIONS);
        instructionsButtonBox.getChildren().add((Node) instructions);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_1_X);
        vbox.setTranslateY(POS_1_Y);
        vbox.getChildren().addAll((Node) play, (Node) stats, (Node) ranking, (Node) options, (Node) exit);

        ((Node) exit).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            account.logout();
            ExitProgram.exitProgram(mainStage);
        });

        ((Node) instructions).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new InstructionScene(mainStage));
        });

        ((Node) stats).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new RankingScene(mainStage));
        });

        ((Node) ranking).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new AbsoluteRankingScene(mainStage));
        });

        ((Node) options).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new OptionScene(mainStage));
        });

        ((Node) play).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox,
                Background.getLogo(), (Node) userLabel, instructionsButtonBox);

        this.setRoot(panel);
    }

}
