package com.geoquiz.view.menu;

import javax.xml.bind.JAXBException;

import com.geoquiz.view.button.*;
import com.geoquiz.view.label.LabelsBuilder;
import com.geoquiz.view.label.MenuLabel;
import com.geoquiz.view.utility.Background;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose difficulty level.
 */
public class LevelScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_Y_BACK = 600;
    private static final double POS_1_X = 100;

    private static final Text BUTTON_PRESSED = new Text();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public LevelScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MenuLabel userLabel = LabelsBuilder.buildUserLabel();

        final Button back;
        final Button easy;
        final Button medium;
        final Button hard;

        back = ButtonsFactory.createButton(Buttons.INDIETRO.toString());
        easy = ButtonsFactory.createButton(ButtonsLevels.FACILE.toString());
        medium = ButtonsFactory.createButton(ButtonsLevels.MEDIO.toString());
        hard = ButtonsFactory.createButton(ButtonsLevels.DIFFICILE.toString());

        VBox vbox = new VBox();
        vbox.getChildren().addAll((Node) easy, (Node) medium, (Node) hard);
        VBox vbox2 = new VBox();
        vbox2.getChildren().add((Node) back);

        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_2_Y);

        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS_Y_BACK);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            mainStage.setScene(new CategoryScene(mainStage));
        });

        ((Node) easy).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            LevelScene.BUTTON_PRESSED.setText(ButtonsLevels.FACILE.toString());
            getLevelPressed();
            try {
                mainStage.setScene(new QuizGamePlay(mainStage));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        ((Node) medium).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            LevelScene.BUTTON_PRESSED.setText(ButtonsLevels.MEDIO.toString());
            getLevelPressed();
            try {
                mainStage.setScene(new QuizGamePlay(mainStage));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        ((Node) hard).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            LevelScene.BUTTON_PRESSED.setText(ButtonsLevels.DIFFICILE.toString());
            getLevelPressed();
            try {
                mainStage.setScene(new QuizGamePlay(mainStage));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(ModeScene.setBackgroundImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);

    }

    /**
     * @return difficulty level.
     */
    static String getLevelPressed() {
        return BUTTON_PRESSED.getText();
    }

}
