package com.geoquiz.view.menu;

import java.io.IOException;
import java.util.Map;

import com.geoquiz.utility.Pair;
import com.geoquiz.view.button.ButtonsCategory;

import com.geoquiz.view.button.ButtonsGameModes;
import com.geoquiz.view.button.ButtonsLevels;
import javafx.stage.Stage;

/**
 * The statistics scene where user can see own records.
 */
public class RankingScene extends AbsoluteRankingScene {

    private Map<Pair<String, String>, Integer> map;

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public RankingScene(final Stage mainStage) {
        super(mainStage);
        super.getTitle().setText("Statistiche");
        try {
            map = super.getRanking().getPersonalRanking(LoginMenuScene.getUsername());
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        super.clearAllLabels();

        super.getCapitalsEasy().setText(super.getCapitalsEasy().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsLevels.FACILE.toString()));
        super.getCapitalsMedium().setText(super.getCapitalsMedium().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsLevels.MEDIO.toString()));
        super.getCapitalsHard().setText(super.getCapitalsHard().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsLevels.DIFFICILE.toString()));
        super.getCapitalsChallenge().setText(super.getCapitalsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.CAPITALI.toString(), ButtonsGameModes.SFIDA.toString()));
        super.getMonumentsEasy().setText(super.getMonumentsEasy().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsLevels.FACILE.toString()));
        super.getMonumentsMedium().setText(super.getMonumentsMedium().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsLevels.MEDIO.toString()));
        super.getMonumentsHard().setText(super.getMonumentsHard().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsLevels.DIFFICILE.toString()));
        super.getMonumentsChallenge().setText(super.getMonumentsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.MONUMENTI.toString(), ButtonsGameModes.SFIDA.toString()));
        super.getFlagsClassic().setText(super.getFlagsClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.BANDIERE.toString(), ButtonsGameModes.CLASSICA.toString()));
        super.getFlagsChallenge().setText(super.getFlagsChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.BANDIERE.toString(), ButtonsGameModes.SFIDA.toString()));
        super.getCurrenciesClassic().setText(super.getCurrenciesClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.VALUTE.toString(), ButtonsGameModes.CLASSICA.toString()));
        super.getCurrenciesChallenge().setText(super.getCurrenciesChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.VALUTE.toString(), ButtonsGameModes.SFIDA.toString()));
        super.getDishesClassic().setText(super.getDishesClassic().getText()
                + this.getRecordByCategory(ButtonsCategory.CUCINA.toString(), ButtonsGameModes.CLASSICA.toString()));
        super.getDishesChallenge().setText(super.getDishesChallenge().getText()
                + this.getRecordByCategory(ButtonsCategory.CUCINA.toString(), ButtonsGameModes.SFIDA.toString()));

    }

    private String getRecordByCategory(final String category, final String difficulty) {
        final Integer record = this.map.get(new Pair<>(category, difficulty));
        return record == null ? "" : record.toString();
    }
}