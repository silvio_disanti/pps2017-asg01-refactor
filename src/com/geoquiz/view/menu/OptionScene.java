package com.geoquiz.view.menu;

import com.geoquiz.view.label.LabelsBuilder;
import com.geoquiz.view.label.MenuLabel;
import com.geoquiz.view.utility.Background;

import java.io.IOException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.Button;
import com.geoquiz.view.button.ButtonsFactory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * The scene where user can change game options.
 */
public class OptionScene extends Scene {

    private static final double POS_2_X = 250;
    private static final double POS_O = 275;
    private static final double POS = 575;
    private static final double POS_1_X = 100;

    private final CheckBox music = new CheckBox("MUSICA");
    private final CheckBox soundEffect = new CheckBox("EFFETTI SONORI");

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public OptionScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final MenuLabel userLabel = LabelsBuilder.buildUserLabel();

        final Button back;
        final Button save;
        back = ButtonsFactory.createButton(Buttons.INDIETRO.toString(), Color.BLUE);
        save = ButtonsFactory.createButton(Buttons.SALVA.toString(), Color.BLUE);

        music.setSelected(!MainWindow.isMusicDisabled());
        music.setStyle("-fx-font-size: 35");
        soundEffect.setSelected(!MainWindow.isClickDisabled());
        soundEffect.setStyle("-fx-font-size: 35");

        VBox vbox = new VBox(music, soundEffect);
        vbox.setTranslateX(POS_2_X);
        vbox.setTranslateY(POS_O);
        VBox vbox2 = new VBox();
        vbox2.setTranslateX(POS_1_X);
        vbox2.setTranslateY(POS);
        vbox2.getChildren().addAll((Node) save, (Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) save).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            this.save();
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), vbox, vbox2,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);

    }

    private void save() {
        if (!music.isSelected()) {
            MainWindow.disableMusic();
        } else {
            MainWindow.enableMusic();
        }
        if (!soundEffect.isSelected()) {
            MainWindow.disableClick();
        } else {
            MainWindow.enableClick();
        }
    }

}
