package com.geoquiz.view.menu;

import com.geoquiz.view.button.Button;
import com.geoquiz.view.button.ButtonsFactory;
import com.geoquiz.view.label.LabelsBuilder;
import com.geoquiz.view.label.MenuLabel;
import com.geoquiz.view.utility.Background;

import java.io.IOException;

import com.geoquiz.view.button.Buttons;
import com.geoquiz.view.button.ButtonsCategory;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The scene where user can choose game category.
 */
public class CategoryScene extends Scene {

    private static final double POS_1_X = 100;
    private static final double POS_1_Y = 450;
    private static final double POS_2_X = 250;
    private static final double POS_2_Y = 300;
    private static final double POS_X_BACK = 450;
    private static final double POS_Y_BACK = 600;
    private static final Text BUTTON_PRESSED = new Text();

    /**
     * @param mainStage
     *            the stage where the scene is called.
     */
    public CategoryScene(final Stage mainStage) {
        super(new StackPane(), mainStage.getWidth(), mainStage.getHeight());

        final Button capitals = ButtonsFactory.createButton(ButtonsCategory.CAPITALI.toString());
        final Button currencies = ButtonsFactory.createButton(ButtonsCategory.VALUTE.toString());
        final Button dishes = ButtonsFactory.createButton(ButtonsCategory.CUCINA.toString());
        final Button monuments = ButtonsFactory.createButton(ButtonsCategory.MONUMENTI.toString());
        final Button flags = ButtonsFactory.createButton(ButtonsCategory.BANDIERE.toString());
        final Button back = ButtonsFactory.createButton(Buttons.INDIETRO.toString());

        final MenuLabel userLabel = LabelsBuilder.buildUserLabel();

        HBox hbox = new HBox(10);
        hbox.setTranslateX(POS_1_X);
        hbox.setTranslateY(POS_1_Y);

        HBox hbox2 = new HBox(10);
        hbox2.setTranslateX(POS_2_X);
        hbox2.setTranslateY(POS_2_Y);

        VBox vbox = new VBox();
        vbox.setTranslateX(POS_X_BACK);
        vbox.setTranslateY(POS_Y_BACK);

        hbox.getChildren().addAll((Node) flags, (Node) currencies, (Node) dishes);
        hbox2.getChildren().addAll((Node) monuments, (Node) capitals);
        vbox.getChildren().add((Node) back);

        ((Node) back).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            try {
                mainStage.setScene(new MainMenuScene(mainStage));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        ((Node) capitals).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            getCategoryPressed(capitals.getText());
            mainStage.setScene(new ModeScene(mainStage));
        });

        ((Node) monuments).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            getCategoryPressed(monuments.getText());
            mainStage.setScene(new ModeScene(mainStage));
        });

        ((Node) currencies).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            getCategoryPressed(currencies.getText());
            mainStage.setScene(new ModeScene(mainStage));
        });

        ((Node) flags).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            getCategoryPressed(flags.getText());
            mainStage.setScene(new ModeScene(mainStage));
        });

        ((Node) dishes).setOnMouseClicked(event -> {
            if (!MainWindow.isClickDisabled()) {
                MainWindow.enableClick();
            }
            getCategoryPressed(dishes.getText());
            mainStage.setScene(new ModeScene(mainStage));
        });

        Pane panel = new Pane();
        panel.getChildren().addAll(Background.getImage(), Background.createBackground(), hbox, hbox2, vbox,
                Background.getLogo(), (Node) userLabel);

        this.setRoot(panel);
    }

    /**
     */
    private static void getCategoryPressed(String category) {
        BUTTON_PRESSED.setText(category);
        BUTTON_PRESSED.getText();
    }

    static String getCategoryPressed() {
        return BUTTON_PRESSED.getText();
    }
}
