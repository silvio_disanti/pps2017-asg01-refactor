package com.geoquiz.view.label;

import com.geoquiz.view.menu.LoginMenuScene;
import javafx.scene.paint.Color;

/**
 * static builder for labels inside menu.
 */
public final class LabelsBuilder {

    private String name;
    private Color color;
    private double fontDimension;

    private LabelsBuilder() {
    }

    public static LabelsBuilder newBuilder() {
        return new LabelsBuilder();
    }

    public LabelsBuilder name(String name) {
        this.name = name;
        return this;
    }

    public LabelsBuilder color(Color color) {
        this.color = color;
        return this;
    }

    public LabelsBuilder fontDimension(double fontDimension) {
        this.fontDimension = fontDimension;
        return this;
    }

    public MenuLabel build() {
        return new MenuLabelImpl(name, color, fontDimension);
    }

    /**
     * Method to build the USER label present in {@link com.geoquiz.view.menu.CategoryScene},
     * {@link com.geoquiz.view.menu.LevelScene}, {@link com.geoquiz.view.menu.MainMenuScene},
     * {@link com.geoquiz.view.menu.ModeScene} and {@link com.geoquiz.view.menu.OptionScene}
     * @return the label to display which user is playing
     */
    public static MenuLabel buildUserLabel() {
        return LabelsBuilder.newBuilder()
                .name("USER: " + LoginMenuScene.getUsername())
                .color(Color.BLACK)
                .fontDimension(40)
                .build();
    }
}
