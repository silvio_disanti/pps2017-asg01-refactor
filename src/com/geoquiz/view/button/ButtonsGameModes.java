package com.geoquiz.view.button;

public enum ButtonsGameModes {
    /**
     * Represents the game mode "Classica".
     */
    CLASSICA,
    /**
     * Represents the game mode "Sfida".
     */
    SFIDA,
    /**
     * Represents the game mode "Allenamento".
     */
    ALLENAMENTO

}
