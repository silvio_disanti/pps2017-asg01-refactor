package com.geoquiz.view.button;

public enum ButtonsLevels {
    /**
     * Represents the difficulty level "Facile".
     */
    FACILE,
    /**
     * Represents the difficulty level "Medio".
     */
    MEDIO,
    /**
     * Represents the difficulty level "Difficile".
     */
    DIFFICILE
}
