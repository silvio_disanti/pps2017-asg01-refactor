package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press to choose category and modality
 * game.
 */
public enum ButtonsCategory {
    /**
     * Represents the category "Capitali".
     */
    CAPITALI,
    /**
     * Represents the category "Valute".
     */
    VALUTE,
    /**
     * Represents the category "Typical dishes".
     */
    CUCINA,
    /**
     * Represents the category "Bandiere".
     */
    BANDIERE,
    /**
     * Represents the category "Monumenti".
     */
    MONUMENTI
}
