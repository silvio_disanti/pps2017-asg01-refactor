package com.geoquiz.view.button;

import javafx.scene.paint.Color;

/**
 * static factory for buttons inside menu.
 */
public final class ButtonsFactory {

    private ButtonsFactory() {
    }

    private static final Color BUTTON_COLOR = Color.BLUE;

    /**
     * 
     * @param name
     *            the text of button.
     *
     * @param colorBackground
     *            the color of button background.
     *
     * @return the button.
     */
    public static Button createButton(final String name, final Color colorBackground) {
        return new ButtonImpl(name, colorBackground);
    }

    /**
     *
     * @param name
     *            the text of button
     * @return a default blue button
     */
    public static Button createButton(final String name) {
        return new ButtonImpl(name, BUTTON_COLOR);
    }

}
